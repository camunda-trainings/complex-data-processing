package com.vminakov.camunda.training.mapping.exception;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Log4j2(topic = "DEFAULT_EXCEPTION_HANDLER")
public class DefaultExceptionHandler {

    @ExceptionHandler(value = {JsonProcessingException.class})
    public void handleJsonProcessingException(JsonProcessingException e) {
        log.error(e.getMessage(), e);
    }
}
