package com.vminakov.camunda.training.mapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.spin.plugin.variable.value.JsonValue;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@Data
public class CustomVariableMapper {
    private final ObjectMapper mapper;
    private final RuntimeService runtimeService;

    public <T> T getValue(DelegateExecution execution, String varName, Class<T> clazz) throws JsonProcessingException {
        Map<String, Object> vars = runtimeService.getVariables(execution.getId());
        if (!vars.containsKey(varName)) {
            return null;
        }

        JsonValue jsonValue = execution.getVariableTyped(varName);
        return mapper.readValue(jsonValue.getValueSerialized(), clazz);
    }

    @SuppressWarnings("unchecked")
    public <T> T getValue(DelegateExecution execution, String name) {
        return (T) runtimeService.getVariable(execution.getId(), name);
    }

    public <T> void saveValue(T value, String name, DelegateExecution execution) {
        runtimeService.setVariable(execution.getId(), name, value);
    }
}
