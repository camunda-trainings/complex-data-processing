package com.vminakov.camunda.training.service;

import com.vminakov.camunda.training.mapping.CustomVariableMapper;
import com.vminakov.camunda.training.model.Person;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
@Log4j2(topic = "STEP_ONE_SERVICE")
@Data
public class StepOneService implements JavaDelegate {

    private final CustomVariableMapper customVariableMapper;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        log.info("step one service");

        Person person = customVariableMapper.getValue(delegateExecution, "person", Person.class);
        log.info("Person object received: {}", person);

        person.setAge(50L);
        log.info("Person object modified: {}", person);
        customVariableMapper.saveValue(person, "person", delegateExecution);

        log.info("data received");
    }
}
