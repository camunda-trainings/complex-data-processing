package com.vminakov.camunda.training.service;

import com.vminakov.camunda.training.mapping.CustomVariableMapper;
import com.vminakov.camunda.training.model.Person;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

@Service
@Log4j2(topic = "STEP_TWO_SERVICE")
@Data
public class StepTwoService implements JavaDelegate {

    private final CustomVariableMapper customVariableMapper;

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        log.info("step two service");

        Person person = customVariableMapper.getValue(delegateExecution, "person");

        log.info("person read from runtime after it was modified at the first step: {}", person);
    }
}
